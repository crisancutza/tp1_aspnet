/*Cancion*/

/* Eliminar Cancion */
create PROCEDURE [dbo].[p_EliminarCancion]
@id integer
AS
BEGIN
      SET NOCOUNT ON;
	  delete from Cancion where IdCancion = @id

      end
/* Fin Eliminar cancion*/

/* Seleccionar cancion por album */
Alter PROCEDURE [dbo].[p_SeleccionarCancionPorAlbum]
@IdAlbum integer
AS
BEGIN
      SET NOCOUNT ON;
      

	 SELECT Cancion.IdCancion,Cancion.Nombre,Cancion.Duracion,Cancion.IdAlbum,
	  CONVERT(date,Cancion.FechaCreacion,106)AS FechaDeCreacion
	  ,Album.Nombre as albunnombre
      FROM Cancion 
	  INNER JOIN Album
ON Cancion.IdAlbum=Album.IdAlbum
    where cancion.IdAlbum = @IdAlbum
      
END/* Fin Seleccionar cancion por album */

/* Listar Cancion*/

alter PROCEDURE [dbo].[p_ListarCanciones]

AS
BEGIN
      SET NOCOUNT ON;
      
      SELECT Cancion.IdCancion,Cancion.Nombre,Cancion.Duracion,Cancion.IdAlbum,
	  CONVERT(date,Cancion.FechaCreacion,106)AS FechaDeCreacion
	  ,Album.Nombre as albunnombre
      FROM Cancion 
	  INNER JOIN Album
ON Cancion.IdAlbum=Album.IdAlbum
    
	  order by Cancion.Nombre asc
      end



/*fin Listar Cancion*/

/* crear Cancion*/


	  	  Alter PROCEDURE [dbo].[p_CrearCancion]
@Nombre varchar(50), @Duracion integer , @IdAlbum integer
AS
BEGIN
	IF NOT EXISTS(select * from Cancion
where IdAlbum = @IdAlbum and Nombre = @Nombre)
	BEGIN
		INSERT INTO dbo.Cancion (Nombre,Duracion,FechaCreacion,IdAlbum) VALUES(@Nombre,@Duracion, GETDATE(), @IdAlbum )
	END
	
ENd



/* fin crea Cancion*/


/* Album*/
/* Listar Album para ddl*/
CREATE PROCEDURE [dbo].[p_SeleccionarAlbum]

AS
BEGIN
      SET NOCOUNT ON;
      
      
      
Select IdAlbum , Nombre from dbo.Album
  
      
END
/*  fin Listar Album para ddl*/

/* Listar Album*/
Alter PROCEDURE [dbo].[p_ListarAlbum]

AS
BEGIN
      SET NOCOUNT ON;
     
      SELECT  IdAlbum, Nombre , Anio 
      FROM Album 
	  where IdAlbum <> '1016' /* Aca crean un Desconocido y le ponen el ID */
      order by Nombre asc
      end
	  
/* Fin Listar Album*/	  

/* Eliminar  Album*/	
	  	  Alter PROCEDURE [dbo].[p_EliminarAlbum]
@id integer
AS
BEGIN
	IF EXISTS(SELECT * FROM Album WHERE IdAlbum = @id)
	BEGIN
		 delete from cancion where IdAlbum = @id
		  delete from album where IdAlbum = @id
	END
	ELSE 
	BEGIN
		delete from album where IdAlbum = @id
	END
ENd
	/* Fin Eliminar  Album*/	  


	  /* Crear  Album*/	
	  
	  ALTER PROCEDURE [dbo].[p_CrearAlbum]
@Nombre varchar(50), @Anio integer
AS
BEGIN
      SET NOCOUNT ON;
      
      INSERT INTO dbo.Album (Nombre,Anio,FechaCreación) VALUES(@Nombre,@Anio, GETDATE())
  
      
END

/* fin Crear  Album*/	

















