﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SinMenu.Master" AutoEventWireup="true" CodeBehind="Registrarse.aspx.cs" Inherits="WebApplication1.Registrarse" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceLogin" runat="server">

<div class="container" >
    <div class="row row-offcanvas row-offcanvas-right" >
        <div class="col-xs-12 col-sm-12">
          <div class="row" >
            <div class="col-12 col-sm-12 col-lg-12">
			    
<form id="form1" runat="server">
    <div>
     <div>
        <asp:ScriptManager ID="SM1" runat="server">
        </asp:ScriptManager>

                    <span id="tp-formulario">Nombre</span><br>
                        <asp:TextBox ID="RegNombre" runat="server"  BackColor="#54B948" BorderStyle="Solid" MaxLength="20"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="" Text="Campo Obligatorio" 
                         ControlToValidate="RegNombre"></asp:RequiredFieldValidator>
                      <br/>

         
                    <span id="tp-formulario">Apellido</span><br>
                     <asp:TextBox ID="RegApellido" runat="server"  BackColor="#54B948" BorderStyle="Solid" MaxLength="20"></asp:TextBox>  <br/>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="" Text="Campo Obligatorio" 
                      ControlToValidate="RegApellido"></asp:RequiredFieldValidator>
                      <br/>

                    <span id="tp-formulario">Mail</span><br>
                    <asp:TextBox ID="RegMail" runat="server"  BackColor="#54B948" BorderStyle="Solid" MaxLength="50"></asp:TextBox><br/>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="" Text="Campo Obligatorio" 
                      ControlToValidate="RegMail"></asp:RequiredFieldValidator>
                     <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="" Text="Formato no valido" ControlToValidate="RegMail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                     <br />


                    <span id="tp-formulario">Contraseña</span><br/>
                    <asp:TextBox ID="RegPass" runat="server"  BackColor="#54B948" BorderStyle="Solid" MaxLength="20"  type="password"></asp:TextBox><br/>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="" Text="Campo Obligatorio" 
                      ControlToValidate="RegPass"></asp:RequiredFieldValidator>

                    <span id="tp-formulario">Verificacion de Contraseña</span><br>
                    <asp:TextBox ID="RegPassVerif" runat="server"  BackColor="#54B948" BorderStyle="Solid" MaxLength="20"   type="password"></asp:TextBox><br>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="" Text="Campo Obligatorio" 
                       ControlToValidate="RegPassVerif"></asp:RequiredFieldValidator>
                   <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Los campos no son iguales" ControlToCompare="RegPass" ControlToValidate="RegPassVerif"></asp:CompareValidator>
                    
                    <br />
               
                    Ingrese Captcha :
                
                    <asp:TextBox ID="txtCaptcha" runat="server" Width="200px"></asp:TextBox>
              
                    <asp:UpdatePanel ID="UP1" runat="server">
                        <ContentTemplate>
                           
                                        <asp:Image ID="imgCaptcha" runat="server" />
                                
                                  
                                        <asp:Button ID="btnRefresh" runat="server" Text="Refresh" OnClick="btnRefresh_Click" />
                                  
                        </ContentTemplate>
                    </asp:UpdatePanel>
             
               
                    <asp:Button ID="btnRegiser" runat="server" Text="Register" OnClick="btnRegister_Click" />
                
    </div>



    </div>
    </form>
            </div>
           </div> 
        </div>
    </div>
</div>


</asp:Content>
