﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication1.Clases;
using System.Data;//agregado
using System.Data.SqlClient;//agregado
using System.Configuration;//agregado



namespace WebApplication1
{
    public partial class CrearAlbum : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblMsjCreacion.Text = "Cree un album nuevo";
        }

        protected void InsertarAlbum_Click(object sender, EventArgs e)
        {

            AlbumService album = new AlbumService();
            Album al = new Album();

            al.Nombre = NombreAlbum.Text;
            al.Anio = Convert.ToInt32(AnoAlbum.Text);
            album.Album_Crear(al);
            lblMsjCreacion.Text = "Muchas gracias album cargado correctamente";
            
            }
        }


        
    }
