﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication1.Clases;

namespace WebApplication1
{
    public partial class EliminarCancion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            CancionService cancionservice = new CancionService();

            int id = Convert.ToInt32( Session["INDEX"]);
                bool ok = cancionservice.Cancion_Eliminar(id);


                if (ok == true)
                {
                    Response.Redirect("ListarCancion.aspx");

                }
            
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Response.Redirect("ListarCancion.aspx");
        }
    }
}