﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication1.Clases;
using System.Data;//agregado
using System.Data.SqlClient;//agregado
using System.Configuration;//agregado

namespace WebApplication1
{
    public partial class ListarAlbum : System.Web.UI.Page
    {
        AlbumService album = new AlbumService();
        protected void Page_Load(object sender, EventArgs e)
        {
           
            DataSet albumes  = album.Album_ObtenerTodas();

            GridView1.DataSource = albumes;
            GridView1.DataBind();

        }


        protected void eliminaralbum_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Eliminar")
            {
                var colsNoVisible = GridView1.DataKeys[0].Values;
                int id =  Convert.ToInt32(colsNoVisible[0]);
                



                Session["eliminaralbum"] = id;

                Response.Redirect("EliminarAlbum.aspx");

               
            }
            
        }

    }
}