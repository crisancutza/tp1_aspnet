﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ConMenu.Master" AutoEventWireup="true" CodeBehind="ListarAlbum.aspx.cs" Inherits="WebApplication1.ListarAlbum" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceCuerpoIndex" runat="server">
 <div class="container" >
        <div class="row row-offcanvas row-offcanvas-right" >
            <div class="col-xs-12 col-sm-12">
                <div class="row"  >
                    <div class="col-12 col-sm-12 col-lg-12">
                        Lista Album
                       <form id="form1" runat="server">
                       
                        <asp:GridView ID="GridView1" runat="server" OnRowCommand="eliminaralbum_RowCommand" AutoGenerateColumns="False" DataKeyNames="IdAlbum" > 
                            <Columns>
                                <asp:BoundField DataField="IdAlbum" HeaderText="ID" Visible="false"  />
                                <asp:BoundField DataField="Nombre" HeaderText="Nombre" Visible="True" />
                                <asp:BoundField DataField="Anio" HeaderText="Año" Visible="True" />
                                <asp:ButtonField CommandName="Eliminar" Text="Eliminar" />
                                 
                            </Columns>
                        </asp:GridView>
                        </form>
                    </div><!--/span-->
                </div><!--/row-->
            </div><!--/span-->
        </div><!--/row-->
    </div>
</asp:Content>
