﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Clases;

namespace WebApplication1.Clases
{
    public class Cancion
    {

        private string nombre;
        private int duracion;
        private DateTime fechaCrecion;
        private int album;

        public string Nombre { get { return nombre; } set { nombre = value; } }
        public int Duracion { get { return duracion; } set { duracion = value; } }
        public int Album { get { return album; } set { album = value; } }
        public DateTime FechaCreacion { get { return fechaCrecion; } set { fechaCrecion = value; } }
    }
}