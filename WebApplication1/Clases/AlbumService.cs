﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using WebApplication1.Clases;
using System.Data;

namespace WebApplication1.Clases
{
    public class AlbumService
    {

         Ado _ado;
	
    
    public AlbumService()
	{
	    _ado = new Ado();
	}

    


    
    public DataSet Album_ObtenerTodas()
    {
        _ado.EjecutarStoredProcedure(false, "p_ListarAlbum", null);

        return _ado.DevolverDatos();

    }


    public bool Album_Eliminar(int id)
    {
        ArrayList parametros = new ArrayList();

        parametros.Add(id);

        return _ado.EjecutarStoredProcedure(false, "p_EliminarAlbum", parametros);
    
    }

    //public bool Album_Crear(string nombre, int ano)
    public bool Album_Crear(Album album)
    {
        ArrayList datos = new ArrayList();
        datos.Add(album.Nombre);
        datos.Add(album.Anio); 

        return _ado.EjecutarStoredProcedure(false, "p_CrearAlbum", datos);

    }

    public DataSet Album_ObtenerNombreAlbum()
    {
        _ado.EjecutarStoredProcedure(false, "p_SeleccionarAlbum", null);

        return _ado.DevolverDatos();

    }


    }
}