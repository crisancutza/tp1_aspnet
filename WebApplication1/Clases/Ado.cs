﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;//agregado
using System.Data.SqlClient;//agregado
using System.Configuration;//agregado
using System.Collections;
using WebApplication1.Clases;

namespace WebApplication1.Clases
{
    public class Ado
    {
        string sConexion;
        SqlCommand sqlComm;
        SqlConnection sqlConn;
        SqlDataAdapter sqlDA;
        DataTable dtResultado;
        SqlTransaction transaccion;
        Int32 cantidadParametros = 0;
        Int32 numeroDeRegistro = 0;

        public Ado()
        {

        }

        private bool Conectar()
        {
            sConexion = CadenaConexion();

            if (sqlConn == null)
                sqlConn = new SqlConnection(sConexion);

            if (sqlConn.State == ConnectionState.Closed)
                sqlConn.Open();


            return (sqlConn.State == ConnectionState.Open);

        }

        private string CadenaConexion()
        {
            SqlConnectionStringBuilder csb = new SqlConnectionStringBuilder();
            csb.DataSource = @"BLAS\SQLEXPRESS";
            csb.InitialCatalog = "TP20142C";
            csb.IntegratedSecurity = true;
            //csb.UserID = "sa";
            //csb.Password = "globons";

            return csb.ConnectionString;
        }

        private bool ComenzarTransaccion()
        {
            try
            {
                transaccion = sqlConn.BeginTransaction();
            }
            catch (SqlException sqlEx)
            {
                return false;
            }

            return true;

        }

        private bool FinalizarTransaccion()
        {
            try
            {
                transaccion.Commit();
            }
            catch (SqlException sqlEx)
            {
                transaccion.Rollback();
                return false;
            }

            return true;

        }

        private bool CancelarTransaccion()
        {
            try
            {
                transaccion.Rollback();
            }
            catch (SqlException sqlEx)
            {
                return false;
            }

            return true;

        }


        public bool EjecutarStoredProcedure(bool tieneTransaccion, string nombreSP, ArrayList sqlParametros)
        {
            if (Conectar())
            {

                sqlComm = new SqlCommand();		// Instancio el objeto Command de la clase
                sqlComm.Connection = sqlConn;	    // Asigno la conexión activa al Command

                sqlComm.CommandType = CommandType.StoredProcedure;	// Indico que se trata de un procedimiento almacenado
                sqlComm.CommandText = nombreSP;		                // Indico cual es el stored procedure

                if (tieneTransaccion)
                    sqlComm.Transaction = transaccion;

                SqlCommandBuilder.DeriveParameters(sqlComm);       // Obtengo los Parametros del SP del SQLServer

                Int32 cantidadParametros;

                if (sqlParametros == null)
                    cantidadParametros = 0;
                else
                    cantidadParametros = sqlParametros.Count;

                if (cantidadParametros == sqlComm.Parameters.Count - 1)
                {
                    for (int i = 1; i <= sqlComm.Parameters.Count - 1; i++)
                    {
                        sqlComm.Parameters[i].Value = sqlParametros[i - 1];		// Agrego el parámetro sqlConn el valor del cod de la provincia para obtener sus localidades
                    }

                    sqlComm.ExecuteNonQuery();

                    return true;
                }

            }

            return false;
        }

        public DataSet DevolverDatos()
        {

            DataSet ds = new DataSet();

            sqlDA = new SqlDataAdapter(sqlComm);

            sqlDA.Fill(ds);

            return ds;


        }


    }
}