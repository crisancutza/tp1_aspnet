﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Clases;

namespace WebApplication1.Clases
{
    public class Album
    {

        private string nombre;
        private int anio;
        private DateTime fechaCrecion;

        public string Nombre { get { return nombre; } set { nombre = value; } }
        public int Anio { get { return anio; } set { anio = value; } }
        public DateTime FechaCreacion { get { return fechaCrecion; } set { fechaCrecion = value; } }




    }
}