﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using WebApplication1.Clases;
using System.Data;
namespace WebApplication1.Clases
{
    public class CancionService
    {


        Ado _ado;


        public CancionService()
        {
            _ado = new Ado();
        }


        public bool Cancion_Crear(Cancion cancion)
        {

            ArrayList datos = new ArrayList();
            datos.Add(cancion.Nombre);
            datos.Add(cancion.Duracion);
            datos.Add(cancion.Album);


            return _ado.EjecutarStoredProcedure(false, "p_CrearCancion", datos);

        }

        public bool Cancion_Eliminar(int id)
        {
            ArrayList parametros = new ArrayList();

            parametros.Add(id);

            return _ado.EjecutarStoredProcedure(false, "p_EliminarCancion", parametros);

        }

        public DataSet Cancion_ObtenerCancionPorAlbum(Cancion cancion)
        {

            ArrayList cancionAlbum = new ArrayList();
            cancionAlbum.Add(cancion.Album);

            _ado.EjecutarStoredProcedure(false, "p_SeleccionarCancionPorAlbum", cancionAlbum);

            return _ado.DevolverDatos();

        }

        public DataSet Cancion_ObtenerTodas()
    {
        _ado.EjecutarStoredProcedure(false, "p_ListarCanciones", null);

        return _ado.DevolverDatos();

    }

        


    }
}