﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication1.Clases;
using System.Data;
namespace WebApplication1
{
    public partial class ListarCancion : System.Web.UI.Page
    {
        AlbumService albumserv = new AlbumService();

        CancionService cancionservice = new CancionService();


                
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) { 
            DDLFiltrar.DataSource = albumserv.Album_ObtenerNombreAlbum();
            DDLFiltrar.DataBind();
            DDLFiltrar.SelectedValue = "1016";
            }


            // Lleno por primera vez el grid con todos los registros
            
            
            
            DataSet albumes = cancionservice.Cancion_ObtenerTodas();

            gvcancion.DataSource = albumes;
            gvcancion.DataBind();
            // fin
        }

        protected void Unnamed1_Click(object sender, EventArgs e)
        {
            Cancion can = new Cancion();
            can.Album = Convert.ToInt32(DDLFiltrar.SelectedValue);
            DataSet albumes = cancionservice.Cancion_ObtenerCancionPorAlbum(can);
            gvcancion.DataSource = albumes;
            gvcancion.DataBind();
    
        }
        protected void eliminarCancion_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            var colsNoVisible = gvcancion.DataKeys[0].Values;
            int id = Convert.ToInt32(colsNoVisible[0]);
             
          
            Session["INDEX"] = id;
              
            Response.Redirect("EliminarCancion.aspx");


        }

    }
}