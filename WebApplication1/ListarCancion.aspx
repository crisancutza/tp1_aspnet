﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ConMenu.Master" AutoEventWireup="true" CodeBehind="ListarCancion.aspx.cs" Inherits="WebApplication1.ListarCancion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceCuerpoIndex" runat="server">
 <div class="container" >
        <div class="row row-offcanvas row-offcanvas-right" >
            <div class="col-xs-12 col-sm-12">
                <div class="row"  >
                    <div class="col-12 col-sm-12 col-lg-12">
                        <form runat="server"> 
                        Filtrar 
                        <asp:DropDownList ID="DDLFiltrar" runat="server" DataTextField="Nombre" DataValueField="IdAlbum"></asp:DropDownList>
                            <asp:Button runat="server" Text="Filtrar" onclick="Unnamed1_Click" ID="filtrar"/>

                            <asp:GridView ID="gvcancion" runat="server" AutoGenerateColumns="False"  OnRowCommand="eliminarCancion_RowCommand" DataKeyNames="IdCancion , IdAlbum " >
                               
                               <Columns> <asp:BoundField DataField="IdCancion" HeaderText="ID" Visible="false"  />
                                <asp:BoundField DataField="Nombre" HeaderText="Nombre" Visible="true"  />
                                <asp:BoundField DataField="Duracion" HeaderText="Duracion" Visible="True" />
                                <asp:BoundField DataField="IdAlbum" HeaderText="IdAlbum" Visible="false" />
                                <asp:BoundField DataField="FechaDeCreacion" HeaderText="FechaCreacion" Visible="True" />
                                <asp:BoundField DataField="albunnombre" HeaderText="Nombre Album" Visible="True" />
                                <asp:ButtonField CommandName="Eliminar" Text="Eliminar" />
                               </Columns>
                            </asp:GridView>
                        

                        </form>
                    </div><!--/span-->
                </div><!--/row-->
            </div><!--/span-->
        </div><!--/row-->
    </div>
</asp:Content>
