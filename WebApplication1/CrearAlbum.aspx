﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ConMenu.Master" AutoEventWireup="true" CodeBehind="CrearAlbum.aspx.cs" Inherits="WebApplication1.CrearAlbum" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceCuerpoIndex" runat="server">
    <div class="container" >
        <div class="row row-offcanvas row-offcanvas-right" >
            <div class="col-xs-12 col-sm-12">
                <div class="row"  >
                    <div class="col-12 col-sm-12 col-lg-12">
                        <form runat="server" id="FormAlbum">
                        <asp:Label ID="lblMsjCreacion" runat="server" Text="Label"></asp:Label><br />
                            <span id="tp-formulario">Nombre</span><br/>
                            <asp:TextBox ID="NombreAlbum" runat="server"  BackColor="#54B948" BorderStyle="Solid" MaxLength="50"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="" Text="Campo Obligatorio" 
                              ControlToValidate="NombreAlbum"></asp:RequiredFieldValidator>
                            <br/>
                            <span id="Span1">Año</span><br/>
                            <asp:TextBox ID="AnoAlbum" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="" Text="Campo Obligatorio" 
                              ControlToValidate="AnoAlbum"></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="" Text="Año no valido" ControlToValidate="AnoAlbum" MinimumValue="1700" MaximumValue="2016"></asp:RangeValidator>
                        <asp:Button ID="InsertarAlbum" runat="server" Text="Button" 
                                onclick="InsertarAlbum_Click" /></form>

                        

                    </div><!--/span-->
                </div><!--/row-->
            </div><!--/span-->
        </div><!--/row-->
    </div>
</asp:Content>
