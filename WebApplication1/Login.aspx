﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SinMenu.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="WebApplication1.Login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceLogin" runat="server">
<div class="container" >

      <div class="row row-offcanvas row-offcanvas-right" >

        <div class="col-xs-12 col-sm-12">
        
          
          <div class="row" >
            <div class="col-12 col-sm-12 col-lg-12">
				 <div class="col-6 col-sm-6 col-lg-6">
					<form method="post" id="loginForm1" action="#" runat="server">
						<span id="tp-formulario">User</span><br>
                        <asp:TextBox ID="userName" name="userName" runat="server" 
                            BackColor="#54B948" BorderStyle="Solid" MaxLength="20"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="userName" ErrorMessage="Faltan datos"></asp:RequiredFieldValidator>

						<span id="tp-formulario">Password</span><br>
                        <asp:TextBox type="password" id="passWord" name="passWord" runat="server"  BackColor="#54B948" BorderStyle="Solid" MaxLength="20" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Faltan datos" ControlToValidate="passWord"></asp:RequiredFieldValidator>
                    <asp:Button ID="tp_login_entrar" runat="server" Text="Button" />
						
					</form>
				</div>	
			
				<div class="col-6 col-sm-6 col-lg-6 " id ="loginFormReg"  >
					<span id="tp-formulario-registrarte">Sino tenes usuarios podes registrarte mediante este link</span><br>
					<a id="tp-letra-blanca" href="Registrarse.aspx">Registrarte</a>
				</div>	
			
                
            </div><!--/span-->
         
           
          
          </div><!--/row-->
        </div><!--/span-->

      </div><!--/row-->

    </div>

</asp:Content>
