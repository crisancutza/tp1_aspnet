﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ConMenu.Master" AutoEventWireup="true" CodeBehind="CrearCancion.aspx.cs" Inherits="WebApplication1.CrearCancion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceCuerpoIndex" runat="server">
 <div class="container" >
        <div class="row row-offcanvas row-offcanvas-right" >
            <div class="col-xs-12 col-sm-12">
                <div class="row"  >
                    <div class="col-12 col-sm-12 col-lg-12">
                       <form runat="server" id="FormCancion">
                       <asp:Label ID="lblmsj" runat="server" Text="Label"></asp:Label>
                       <br />
                            <span id="tp-formulario">Nombre</span><br/>
                            <asp:TextBox ID="NombreCancion" runat="server"  BackColor="#54B948" BorderStyle="Solid" MaxLength="50"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="" Text="Campo Obligatorio" 
                              ControlToValidate="NombreCancion"></asp:RequiredFieldValidator>
                            <br/>
                            <span id="Span1">Duracion</span><br/>
                            <asp:TextBox  runat="server" ID="DuracionCancion"></asp:TextBox>
                             <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="" Text="Campo Vacio" ControlToValidate="DuracionCancion"></asp:RequiredFieldValidator>
                       
                                                      
                            <br/>
                            <span id="Span2">Album</span><br/>
                             <asp:DropDownList ID="DDLAlbum" runat="server" DataTextField="Nombre" DataValueField="IdAlbum" >
                             <asp:ListItem Selected="True" Value="0">Desconocido</asp:ListItem>
                            </asp:DropDownList>
                            <asp:Button ID="Button1" runat="server" Text="Button" onclick="Button1_Click" />
                        </form>
                    </div><!--/span-->
                </div><!--/row-->
            </div><!--/span-->
        </div><!--/row-->
    </div>
</asp:Content>
