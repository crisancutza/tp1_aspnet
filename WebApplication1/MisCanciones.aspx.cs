﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class MisCanciones : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["usuario"] != null)
            {
                string usuario = (string)Session["usuario"];
                Literal1.Text = string.Format("<h2>{0}</h2>", usuario);
            }
            else { Literal1.Text = "No registrado"; }
        }
    }
}