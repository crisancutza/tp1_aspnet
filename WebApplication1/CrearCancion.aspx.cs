﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication1.Clases;

namespace WebApplication1
{
    public partial class CrearCancion : System.Web.UI.Page
    {
        AlbumService albumserv = new AlbumService();
        CancionService cancionservice = new CancionService();
       


        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack) { 
               
            DDLAlbum.DataSource = albumserv.Album_ObtenerNombreAlbum();
               
            DDLAlbum.DataBind();
            DDLAlbum.SelectedValue = "1016";
            lblmsj.Text = "Cree una cancion";
            }
            

        }

        protected void Button1_Click(object sender, EventArgs e)

        {
            Cancion can = new Cancion();
           
            can.Nombre = NombreCancion.Text;
          can.Duracion = Convert.ToInt32(DuracionCancion.Text);
          can.Album = Convert.ToInt32(DDLAlbum.SelectedValue);

            cancionservice.Cancion_Crear(can);
            lblmsj.Text = "Muchas gracias, Verifique si su cancion se creo";
        }
    }
}